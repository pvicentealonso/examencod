/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package examencontornosej1;

import java.util.Map;
import java.util.TreeMap;
import javax.swing.JOptionPane;

/**
 *
 * @author hp
 */
public class ExamenContornosEj1 {

    /*
Realiza un programa que inserte en un TreeMap, cinco productos (1 punto).

Los productos son objetos, que tienen nombre y precio, además de los métodos set y get correspondientes para estos atributos. (-2 puntos si no se hace)

La clave del TreeMap es el número de serie. (-1 punto si no se hace)

Luego de insertarlos, liste por pantalla los cinco productos (2 puntos).

Envía el código correspondiente comentado para generar javadoc (1 punto).
     */
    
    /**
     * @param numeroProductos Número de productos a generar.
     * @param i Número del iterador que definirá el código de cada producto generado.
     * @param precio Precio del producto generado.
     * @param nombre Nombre del producto generado.
     */
    
    public static void main(String[] args) {       
       
        // 1 - Creamos el objeto TreeMap que deseamos.
        TreeMap<Integer, Producto> tree = new TreeMap<Integer, Producto>();
        
        // 2 - Pedimos número de objetos a meter.
        int numeroProductos = Integer.valueOf(JOptionPane.showInputDialog("¿Cuantos productos quieres meter?"));
        
        // 3 - Creación objetos de producto y metido en el TreeMap.
        for(int i=0;i<numeroProductos;i++){
          String nombre = JOptionPane.showInputDialog("Escribe el nombre del producto");
          int precio = Integer.valueOf(JOptionPane.showInputDialog("Inserta el precio"));
          Producto producto = new Producto(i, nombre, precio);         
          tree.put(producto.getNumeroSerie(),producto);
        }
        
        // 4 - Muestreo en pantalla.
        for(int i=0;i<numeroProductos;i++){
            System.out.println("Código: "+tree.get(i).getNumeroSerie()+" || Nombre: "+tree.get(i).getNombre()+" || Precio: "+tree.get(i).getPrecio()+" €");
            
                   
        }
        
        
        
        
       
    }
}
