/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package examencontornosej1;

/**
 *
 * @author hp
 */
public class Producto {
    
    private String nombre;
    private int precio;
    private int numeroSerie;
    
    Producto(int numSerie, String nombre, int precio){
        this.numeroSerie = numSerie;
        this.precio = precio;
        this.nombre = nombre;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the precio
     */
    public int getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(int precio) {
        this.precio = precio;
    }

    /**
     * @return the numeroSerie
     */
    public int getNumeroSerie() {
        return numeroSerie;
    }

    /**
     * @param numeroSerie the numeroSerie to set
     */
    public void setNumeroSerie(int numeroSerie) {
        this.numeroSerie = numeroSerie;
    }
    
}
